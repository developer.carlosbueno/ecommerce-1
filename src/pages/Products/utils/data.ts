import { IProduct, ISizes } from "../models/IProduct";

export const products: IProduct[] = []

export const sizes: ISizes[] = [
  { name: 'sm', qty: 0 },
  { name: 'md', qty: 0 },
  { name: 'lg', qty: 0 },
  { name: 'xl', qty: 0 },
  { name: 'xxl', qty: 0 },
]