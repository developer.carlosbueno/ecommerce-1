export interface ProductQueries {
  search?: string
  category?: string
  brand?: string
  sizes?: string[]
  price?: string
}