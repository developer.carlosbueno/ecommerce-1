export { default as ClientsTable } from './ClientsTable';
export { default as OrdersTable } from './OrdersTable';
export { default as PaymentMethodsTable } from './PaymentMethodsTable';
export * from './index';
