export interface ISpend {
  title: string
  description: string
  cost: number
}