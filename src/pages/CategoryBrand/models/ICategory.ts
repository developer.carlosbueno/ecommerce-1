export interface ICategory {
  _id?: string
  name: string
  subcategories: string[]
}