import { IBrand } from "../models/IBrand";
import { ICategory } from "../models/ICategory";

export const categories: ICategory[] = []

export const brands: IBrand[] = [
  { _id: '1', name: 'Adidas', },
  { _id: '2', name: 'Nike', },
  { _id: '3', name: 'Sara', }
]