export { default as OrdersTable } from './ShoppingTable';
export { default as PaymentMethodsTable } from './PaymentMethodsTable';
export * from './index';
