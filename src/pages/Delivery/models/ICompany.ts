export interface ICompany {
  _id?: string
  name: string
  price: number
  disable?: boolean
}