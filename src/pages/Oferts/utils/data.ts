import { IOfert } from "../models/IOfert";

export const ofertsData: IOfert[] = [
  {
    _id: '1',
    code: 'SUM23',
    description: 'Pasa vacaciones con lo mejor del mercado',
    category: 'all',
    brand: 'adidas',
    provinces: ['Santo Domingo'],
    discount: '12',
    expireDate: '27/10/2023',
    disable: false
  },
  {
    _id: '2',
    code: 'SUM22',
    description: 'Pasa vacaciones con lo mejor del mercado',
    category: 'all',
    brand: 'adidas',
    provinces: ['Santo Domingo'],
    discount: '12',
    expireDate: '27/10/2023',
    disable: false
  },
  {
    _id: '3',
    code: 'SUM21',
    description: 'Pasa vacaciones con lo mejor del mercado',
    category: 'all',
    brand: 'adidas',
    provinces: ['Santo Domingo'],
    discount: '12',
    expireDate: '27/10/2023',
    disable: false
  },
]