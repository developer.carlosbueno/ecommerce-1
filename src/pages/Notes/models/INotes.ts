export interface INotes {
  _id?: string
  title: string
  description: string
  label: string
  favorite: boolean
}