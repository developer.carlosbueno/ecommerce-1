export interface IProvincesReport {
  provincesQty: [{
    name: string
    qty: number
    amount: number
  }]
}