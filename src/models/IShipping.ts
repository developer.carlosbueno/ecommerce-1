export interface IShipping {
  province: string
  phone: number
  street1: string
  street2: string
  help: string
  delivery: string
}